<?php declare(strict_types=1);
/*
 * This file is part of "irstea/maybe".
 * (c) 2019 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\Maybe\Tests\Maybe;

use Irstea\Maybe\Exception\NothingException;
use Irstea\Maybe\Maybe\Just;
use Irstea\Maybe\Maybe\Nothing;
use PHPUnit\Framework\TestCase;

/**
 * Class NothingTest.
 */
class NothingTest extends TestCase
{
    /**
     * @testdox ->map() should ignore its argument and return itself.
     */
    public function testMap()
    {
        self::assertEquals(
            [],
            Nothing::instance()->map(
                function ($value) {
                    return $value + 8;
                }
            )->toArray()
        );
    }

    /**
     * @testdox ->resolve() should always call the second callback.
     */
    public function testResolve()
    {
        self::assertEquals(
            0,
            Nothing::instance()->resolve(
                function ($value) {
                    return $value + 8;
                },
                function () {
                    return 0;
                }
            )
        );
    }

    /**
     * @testdox ->toArray() should return an empty array.
     */
    public function testToArray()
    {
        self::assertEquals([], Nothing::instance()->toArray());
    }

    /**
     * @testdox ->value() should throw a NothingException.
     */
    public function testValue()
    {
        $this->expectException(NothingException::class);

        Nothing::instance()->value();
    }

    /**
     * @testdox ->filter() should ignore its argument and return itself.
     */
    public function testFilter()
    {
        self::assertEquals([], Nothing::instance()->filter('is_string')->toArray());
    }

    /**
     * @testdox ->valueOr() should return its argument.
     */
    public function testValueOr()
    {
        self::assertEquals(2, Nothing::instance()->valueOr(2));
    }

    /**
     * @testdox ->concat() should return its argument.
     */
    public function testConcat()
    {
        self::assertEquals([2], Nothing::instance()->concat(Just::from(2))->toArray());
    }

    /**
     * @testdox ->getIterator() should return an empty Iterator.
     */
    public function testGetIterator()
    {
        $values = \iterator_to_array(Nothing::instance());
        self::assertEquals([], $values);
    }
}
