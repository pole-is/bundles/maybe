<?php declare(strict_types=1);
/*
 * This file is part of "irstea/maybe".
 * (c) 2019 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\Maybe\Tests\Maybe;

use Irstea\Maybe\Maybe\Just;
use PHPUnit\Framework\TestCase;

/**
 * Class JustTest.
 */
class JustTest extends TestCase
{
    /**
     * @testdox ->map() should call the callback with the value and pass the result.
     */
    public function testMap()
    {
        self::assertEquals(
            10,
            Just::from(2)->map(
                function ($value) {
                    return $value + 8;
                }
            )->value()
        );
    }

    /**
     * @testdox ->resolve() should call the first callback with the value and pass the result.
     */
    public function testResolve()
    {
        self::assertEquals(
            10,
            Just::from(2)->resolve(
                function ($value) {
                    return $value + 8;
                },
                function () {
                    return 0;
                }
            )
        );
    }

    /**
     * @testdox ->toArray() should return an array of one item.
     */
    public function testToArray()
    {
        self::assertEquals([2], Just::from(2)->toArray());
    }

    /**
     * @testdox ->value() should return the value.
     */
    public function testValue()
    {
        self::assertEquals(
            2,
            Just::from(2)->value()
        );
    }

    /**
     * @testdox ->filter() should filter should return Just when the predicate returns true.
     */
    public function testFilterAccepted()
    {
        self::assertEquals([2], Just::from(2)->filter('is_integer')->toArray());
    }

    /**
     * @testdox ->filter() should filter should return Nothing when the predicate returns false.
     */
    public function testFilterRejected()
    {
        self::assertNotEquals([2], Just::from(2)->filter('is_string')->toArray());
    }

    /**
     * @testdox ->valueOr() should return the value.
     */
    public function testValueOr()
    {
        self::assertEquals(2, Just::from(2)->valueOr(5));
    }

    /**
     * @testdox ->value() should return the value.
     */
    public function testConcat()
    {
        self::assertEquals(2, Just::from(2)->concat(Just::from(6))->value());
    }

    /**
     * @testdox ->getIterator() should return an Iterator with a single item.
     */
    public function testGetIterator()
    {
        $values = \iterator_to_array(Just::from(2));
        self::assertEquals([2], $values);
    }
}
