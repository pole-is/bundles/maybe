<?php declare(strict_types=1);
/*
 * This file is part of "irstea/maybe".
 * (c) 2019 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\Maybe\Tests;

use Irstea\Maybe\Maybe;
use Irstea\Maybe\Maybe\Just;
use Irstea\Maybe\Maybe\Nothing;
use PHPUnit\Framework\TestCase;

/**
 * Class MaybeTest.
 */
class MaybeTest extends TestCase
{
    /**
     * @testdox ::just() should wrap any value with Just::from.
     */
    public function testJustFromScalar()
    {
        self::assertEquals(42, Maybe::just(42)->value());
    }

    /**
     * @testdox ::just() should return any Just instance as is.
     */
    public function testJustFromJust()
    {
        self::assertEquals(42, Maybe::just(Just::from(42))->value());
    }

    /**
     * @testdox ::just() should return Nothing as is.
     */
    public function testJustFromNothing()
    {
        self::assertEquals([], Maybe::just(Nothing::instance())->toArray());
    }

    /**
     * @testdox ::nothing() should return Nothing.
     */
    public function testNothing()
    {
        self::assertEquals([], Maybe::nothing()->toArray());
    }

    /**
     * @testdox ::some() should  the value of Justs.
     */
    public function testSome()
    {
        self::assertEquals(
            [1 => 5, 3 => 'x'],
            Maybe::some(
                [
                    Maybe::nothing(),
                    Maybe::just(5),
                    Maybe::nothing(),
                    Maybe::just('x'),
                    Maybe::nothing(),
                ]
            )
        );
    }

    /**
     * @testdox ::run() should pass back Just values to the generator.
     */
    public function testRunJusts()
    {
        $result = Maybe::run(static function () {
            $valueA = yield Maybe::just(5);
            $valueB = yield Maybe::just(7);
            $valueC = yield Maybe::just(8);

            return $valueA + $valueB + $valueC;
        });

        self::assertEquals(20, $result->valueOr(0));
    }

    /**
     * @testdox ::run() should interrupt the generator when yielded Nothing.
     */
    public function testRunNothing()
    {
        $result = Maybe::run(static function () {
            $valueA = yield Maybe::just(5);
            $valueB = yield Maybe::nothing();
            $valueC = yield Maybe::just(8);

            return $valueA + $valueB + $valueC;
        });

        self::assertEquals(0, $result->valueOr(0));
    }
}
