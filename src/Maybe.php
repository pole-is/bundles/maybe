<?php declare(strict_types=1);
/*
 * This file is part of "irstea/maybe".
 * (c) 2019 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\Maybe;

use Assert\Assertion;
use Irstea\Maybe\Exception\NothingException;
use Irstea\Maybe\Maybe\Just;
use Irstea\Maybe\Maybe\Nothing;

/**
 * Class Maybe.
 */
abstract class Maybe implements \IteratorAggregate
{
    /**
     * @param callable $project
     *
     * @return Maybe
     */
    abstract public function map(callable $project): Maybe;

    /**
     * @param callable $predicate
     *
     * @return Maybe
     */
    abstract public function filter(callable $predicate): Maybe;

    /**
     * @param Maybe $other
     *
     * @return Maybe
     */
    abstract public function concat(Maybe $other): Maybe;

    /**
     * @return \Iterator
     */
    abstract public function getIterator(): \Iterator;

    /**
     * @param callable $withJust
     * @param callable $withNothing
     *
     * @return mixed
     */
    abstract public function resolve(callable $withJust, callable $withNothing);

    /**
     * @param mixed $default
     *
     * @return mixed
     */
    abstract public function valueOr($default);

    /**
     * @throws NothingException
     *
     * @return mixed
     */
    abstract public function value();

    /**
     * @return array
     */
    abstract public function toArray(): array;

    /**
     * @param Maybe|mixed $value
     *
     * @return Maybe
     */
    public static function just($value): Maybe
    {
        return $value instanceof self ? $value : Just::from($value);
    }

    /**
     * @return Maybe
     */
    public static function nothing(): Maybe
    {
        return Nothing::instance();
    }

    /**
     * @param Maybe[] $maybes
     *
     * @return array
     */
    public static function some(array $maybes): array
    {
        Assertion::allIsInstanceOf($maybes, self::class);

        return array_map(
            static function (Just $just) { return $just->value(); },
            array_filter(
                $maybes,
                static function (Maybe $maybe) { return $maybe instanceof Just; }
            )
        );
    }

    /**
     * @param callable $generator
     *
     * @return Maybe
     */
    public static function run(callable $generator): Maybe
    {
        $gen = $generator();
        if (!$gen instanceof \Generator) {
            return Maybe::just($gen);
        }

        try {
            $gen->rewind();
            $maybe = $gen->current();
            while ($gen->valid()) {
                try {
                    $maybe = $gen->send($maybe->value());
                } catch (NothingException $ex) {
                    $gen->throw($ex);
                }
            }

            return Maybe::just($gen->getReturn());
        } catch (NothingException $ex) {
            return Nothing::instance();
        }
    }
}
