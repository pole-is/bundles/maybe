<?php declare(strict_types=1);
/*
 * This file is part of "irstea/maybe".
 * (c) 2019 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\Maybe\Maybe;

use Irstea\Maybe\Exception\NothingException;
use Irstea\Maybe\Maybe;

/**
 * Class Nothing.
 */
final class Nothing extends Maybe
{
    private function __construct()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function map(callable $project): Maybe
    {
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function filter(callable $predicate): Maybe
    {
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function concat(Maybe $other): Maybe
    {
        return $other;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator(): \Iterator
    {
        return new \EmptyIterator();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(callable $withJust, callable $withNothing)
    {
        return $withNothing();
    }

    /**
     * {@inheritdoc}
     */
    public function valueOr($default)
    {
        return $default;
    }

    /**
     * {@inheritdoc}
     */
    public function value()
    {
        throw new NothingException('Nothing has no value');
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return [];
    }

    /**
     * Return the Nothing singleton.
     *
     * @internal
     *
     * @return Nothing
     */
    public static function instance(): Nothing
    {
        /** @var Nothing|null $instance */
        static $instance;
        if (!$instance) {
            $instance = new Nothing();
        }

        return $instance;
    }
}
