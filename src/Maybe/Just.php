<?php declare(strict_types=1);
/*
 * This file is part of "irstea/maybe".
 * (c) 2019 Irstea <dsi.poleis@irstea.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\Maybe\Maybe;

use Irstea\Maybe\Maybe;

/**
 * Class Just.
 */
final class Just extends Maybe
{
    /** @var mixed */
    private $value;

    /**
     * Just constructor.
     *
     * @param mixed $value
     */
    private function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function map(callable $project): Maybe
    {
        return Maybe::just($project($this->value()));
    }

    /**
     * {@inheritdoc}
     */
    public function filter(callable $predicate): Maybe
    {
        return $predicate($this->value) ? $this : Maybe::nothing();
    }

    /**
     * {@inheritdoc}
     */
    public function concat(Maybe $other): Maybe
    {
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator(): \Iterator
    {
        return new \ArrayIterator([$this->value]);
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(callable $withJust, callable $withNothing)
    {
        return $withJust($this->value);
    }

    /**
     * {@inheritdoc}
     */
    public function valueOr($default)
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return [$this->value];
    }

    /**
     * @param mixed $value
     *
     * @internal
     *
     * @return Just
     */
    public static function from($value): Just
    {
        return new Just($value);
    }
}
